package task02_03;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    static List<Calc> calcList = new ArrayList<>();

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        calcList.add(new HexCalc());
        calcList.add(new OctaCalc());

        for (Calc calc : calcList) {

            System.out.println(calc.getClass().getName());
            calc.calculate(1000);
        }

        serialize();
        List<Calc> list = deserialize();
        for (Calc calc : list) {
            System.out.println(calc.getS());
        }

        System.out.println(HexCalc.c.number);
        System.out.println(OctaCalc.c.number);

    }

    public static void serialize() throws IOException {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("output.txt"));
        os.writeObject(calcList);
        os.flush();
        os.close();
        os = new ObjectOutputStream(new FileOutputStream("calc.txt"));
        for (Calc calc : calcList) {

            os.writeObject(calc);
        }
        os.flush();
        os.close();
    }

    public static List deserialize() throws IOException, ClassNotFoundException {

        List<Calc> list = new ArrayList<>();
        ObjectInputStream is = new ObjectInputStream(new FileInputStream("calc.txt"));
        list.add((Calc) is.readObject());
        list.add((Calc) is.readObject());
        return list;
    }

}

interface Calc {
    String getS();

    void calculate(int number);
}

class HexCalc implements Calc, Serializable {
    private String s = "HexCalc";
    static C c;

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public HexCalc() {
        c = new C();
    }

    @Override
    public void calculate(int intNumber) {
        System.out.println(Integer.toHexString(intNumber));
    }
}

class OctaCalc implements Calc, Serializable {
    private String s = "OctaCalc";

    static C c;

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public OctaCalc() {
        c = new C();
    }

    @Override
    public void calculate(int intNumber) {
        System.out.println(Integer.toOctalString(intNumber));
    }
}

class C {
    public static int number = 10;
}