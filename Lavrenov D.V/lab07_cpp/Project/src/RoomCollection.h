
#ifndef SRC_ROOMCOLLECTION_H_
#define SRC_ROOMCOLLECTION_H_
#include "StudyRoom.h"
#include "fstream"

class RoomCollection {
public:
	
	RoomCollection(int size);
	
	RoomCollection(StudyRoom** col, int size);
	
	~RoomCollection();
	
	void insert(StudyRoom *element, const unsigned int index);
	
	void erase(const unsigned int index);
	
	bool setElement(StudyRoom* room, const int index);
	
	StudyRoom* getElement(const int index) const;
	
	StudyRoom* operator[](const unsigned int index);
	
	unsigned int getSize() const;
	
	StudyRoom** getVector() const;
	
	void OnStore();
	
	void OnLoad();
private:
	StudyRoom** vector;
	unsigned int size;

};

#endif /* SRC_ROOMCOLLECTION_H_ */
