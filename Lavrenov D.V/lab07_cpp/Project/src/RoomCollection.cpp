
#include <cstdlib>
#include "RoomCollection.h"
#include "CompareMoreOrEqual.hpp"

RoomCollection::RoomCollection(int size) {
	this->size = size;
	vector = new StudyRoom*[size];
	for (int i = 0; i < size; i++) {
			vector[i] = new StudyRoom();
	}
}

RoomCollection::RoomCollection(StudyRoom** col, int size) {
	this->size = size;
	vector = col;
}

void RoomCollection::insert(StudyRoom *element, const unsigned int index) {
	if (index > 0 && index <= size) {
		StudyRoom** tmp = new StudyRoom*[size];
		for(unsigned int i = index; i <= size; i++)
		tmp[i] = vector[i-1];
		vector[index-1] = element;
		for(unsigned int i = index; i <= size; i++)
			vector[i] = tmp[i];
		size++;
	delete [] tmp;
	}

}
void RoomCollection::erase(const unsigned int index) {
	if (index > 0 && index <= size) {
		for (int i = index - 1; i < size; i++) {
			vector[i] = vector[i + 1];
		}
		vector[size - 1] = NULL;
		size--;
	}
}

bool RoomCollection::setElement(StudyRoom* room, const int index) {
	if (index < size) {
		vector[index] = room;
		return true;
	}
	return false;
}
StudyRoom* RoomCollection::getElement(const int index) const {
	if (index < size)
		return vector[index];
	return NULL;
}
StudyRoom* RoomCollection::operator[](const unsigned int index) {

	return getElement(index);
}



unsigned int RoomCollection::getSize() const {
	return size;
}

StudyRoom** RoomCollection::getVector() const {
	return vector;
}



RoomCollection::~RoomCollection() {
	for (int i = 0; i < size; i++) {
		delete vector[i];
	}
	delete[] vector;
}





