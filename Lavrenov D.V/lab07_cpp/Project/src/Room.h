
#include<iostream>
#include <cstdlib>
#
using namespace std;

#ifndef SRC_ROOM_H_
#define SRC_ROOM_H_
/**
 * Class specifying room params
 * @param length length of the room
 * @param width width of the room
 * @param height height of the room
 */
class Room {
public:

	/**
	 * default constructor
	 */
	Room();
	/**
	 * Copy constructor
	 * @param Room for copying
	 */
	Room(const Room& room);
	/**
	 * getter for length
	 * @return actual length
	 */
	unsigned int getLength() const;
	/**
	 * getter for height
	 * @return actual height
	 */
	unsigned int getHeight() const;
	/**
	 * getter for width
	 * @return actual width
	 */
	unsigned int getWidth() const;
	/**
	 * setter for length
	 * @param length new length
	 */
	void setLength(const unsigned int length);
	/**
	 * setter for height
	 * @param height new height
	 */
	void setHeight(const unsigned int height);
	/**
	 * setter for width
	 * @param width new width
	 */
	void setWidth(const unsigned int width);
	/**
	 * Computing of volume
	 * @return room volume
	 */
	unsigned int volume() const;
	bool operator<(const Room &room);
	/**
	 * destructor
	 */
	virtual ~Room();
protected:
	unsigned int length;
	unsigned int width;
	unsigned int height;

};

#endif /* SRC_ROOM_H_ */
