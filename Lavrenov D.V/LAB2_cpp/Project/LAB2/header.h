/*
=====================
Name : Ipz1
Author: Lavrenov Denis
Description: Wheel declaration
=====================
*/
#include<cmath>
#include "iostream"
#include "stdio.h"
#include "string"
using namespace std;

//����c ������
class CWheel{
private:
	int iDiametr;
	int iWidth;
	string sUnit;
public:
	CWheel();
	~CWheel();
	void  SetDiam(int aDimetr);
	void SetWidth(int aWidth);
	void SetUnit(string aUnit);
	int GetDiam() const;
	int GetWidth() const;
	string GetUnit() const;
	const int Volume(const CWheel& iWheel) const;
};

class CView{
public:
	void SetDataSource(CWheel* iWheel);
	void PrintData(const CWheel& iWheel);
};