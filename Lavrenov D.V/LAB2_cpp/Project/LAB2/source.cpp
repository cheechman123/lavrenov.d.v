#include "header.h"

CWheel::CWheel(){
	cout<<"constructor  is worked"<<endl;
}

CWheel::~CWheel(void){
	cout<<"destructor is worked"<<endl;
}

void CWheel::SetDiam(int aDiametr) {
	iDiametr = aDiametr;
}
void CWheel::SetWidth(int aWidth){
	 iWidth = aWidth;
}
void CWheel::SetUnit(string aUnit){
	sUnit = aUnit;
}
 int CWheel::GetDiam() const{
	return iDiametr;
}
int CWheel::GetWidth() const{
	return iWidth;
}
 string CWheel::GetUnit() const{
	return sUnit;
}

void CView::SetDataSource( CWheel* iWheel) {
	iWheel->SetDiam(40);
	iWheel->SetWidth(80);
	iWheel->SetUnit("����");
}

void CView::PrintData(const CWheel& iWheel){
	cout<<"������� "<<iWheel.GetDiam()<<endl;
	cout<<"������ "<<iWheel.GetWidth()<<endl;
	cout<<"������� ��������� "<<iWheel.GetUnit()<<endl;
}

const int CWheel::Volume(const CWheel& iWheel) const{
	double pi = 3.14;
	int  radius = iWheel.GetDiam()/2;
	int shirina = iWheel.GetWidth();
	double volume = pi*radius*radius*shirina;
	cout<<"  �����   "<<volume<<" "<<iWheel.GetUnit()<<endl;
	return 0;
}