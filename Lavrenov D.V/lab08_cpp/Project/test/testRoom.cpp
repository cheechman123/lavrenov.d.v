/*
 * test.cpp
 *
 *  Created on: 12 ���. 2018 �.
 *      Author: Daria
 */

#include "gtest/gtest.h"

#include "../src/Room.h"

TEST (Room, shouldDefaultInitializeRoom) {

	Room room;
	int expectedLength = 0;
	int expectedWidth = 0;
	int expectedHeight = 0;
	int actualLength = room.getLength();
	int actualHeight = room.getHeight();
	int actualWidth = room.getWidth();
	EXPECT_TRUE(expectedLength==actualLength);
	EXPECT_TRUE(expectedHeight==actualHeight);
	ASSERT_TRUE(expectedWidth==actualWidth);

}

TEST (Room, shouldCalculateVolume) {

	Room room;
	unsigned int height = 10;
	unsigned int width = 20;
	unsigned int length = 30;
	room.setHeight(height);
	room.setWidth(width);
	room.setLength(length);
	int expectedVolume = 6000;
	int actualVolume = room.volume();
	ASSERT_TRUE(expectedVolume==actualVolume);

}

TEST (Room, shouldSetHeight) {
	Room room;
	unsigned int expectedHeight=10;
	room.setHeight(expectedHeight);
	unsigned int actualHeight = room.getHeight();
	ASSERT_TRUE(expectedHeight==actualHeight);
}

TEST (Room, shouldSetLength) {
	Room room;
	unsigned int expectedLength=10;
	room.setLength(expectedLength);
	unsigned int actualLength = room.getLength();
	ASSERT_TRUE(expectedLength==actualLength);
}

TEST (Room, shouldSetWidth) {
	Room room;
	unsigned int expectedWidth=10;
	room.setWidth(expectedWidth);
	unsigned int actualWidth = room.getWidth();
	ASSERT_TRUE(expectedWidth==actualWidth);
}

TEST (Room, shouldCompareUsingOverridedOperator) {
	Room r1;
	r1.setHeight(10);
	r1.setWidth(10);
	r1.setLength(10);
	Room r2;
	r2.setHeight(20);
	r2.setWidth(20);
	r2.setLength(20);
	ASSERT_TRUE(r1 < r2);

}
