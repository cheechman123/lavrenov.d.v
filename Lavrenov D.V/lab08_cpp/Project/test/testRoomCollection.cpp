/*
 * testRoomCollection.cpp
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: Daria
 */

#include "gtest/gtest.h"

#include "../src/RoomCollection.h"



TEST(RoomCollection, shouldInitialize) {
	unsigned int size = 10;
	RoomCollection collection(size);
	for (int i = 0; i < size; i++)
		EXPECT_TRUE(collection.getElement(i)!=NULL);
}

TEST (RoomCollection, shouldSetElement) {
	unsigned int size = 10;
	int index = 3;
	RoomCollection collection(size);
	StudyRoom *room = new StudyRoom();
	int expectedWidth = 10;
	room->setWidth(expectedWidth);
	collection.setElement(room, index);
	int actualWidth = collection.getElement(index)->getWidth();
	ASSERT_TRUE(expectedWidth == actualWidth);

}

TEST (RoomCollection, shouldUseOverridedSquareBrackets) {
	unsigned int size = 10;
	int index = 3;
	RoomCollection collection(size);
	StudyRoom *room = new StudyRoom();
	int expectedWidth = 10;
	room->setWidth(expectedWidth);
	collection.setElement(room, index);
	Room* actualRoom = collection[index];
	int actualWidth = actualRoom->getWidth();
	ASSERT_TRUE(expectedWidth == actualWidth);

}

TEST (RoomCollection, shouldReturnNullOutOfRange) {
	unsigned int size = 10;
	int index = 12;
	RoomCollection collection(size);
	Room* actualRoom = collection.getElement(index);
	Room* expectedRoom = NULL;
	ASSERT_TRUE(actualRoom == expectedRoom);
}

TEST(RoomColelection, shouldNotSetOutOfRange) {

	unsigned int size = 10;
	int index = 12;
	RoomCollection collection(size);
	StudyRoom* room = new StudyRoom();
	bool actual = collection.setElement(room, index);
	bool expected = false;
	ASSERT_TRUE(actual == expected);

}

TEST(RoomCollection, shouldIncrementSizeAfterInsert) {

	unsigned int size = 10;
	int index = 1;
	RoomCollection collection(size);
	StudyRoom* room = new StudyRoom();
	collection.insert(room, index);
	int expectedSize = size + 1;
	int actualSize = collection.getSize();
	ASSERT_TRUE(actualSize == expectedSize);

}

TEST(RoomCollection, shouldDecrementSizeAfterErase) {

	unsigned int size = 10;
	int index = 1;
	RoomCollection collection(size);
	collection.erase(index);
	int expectedSize = size - 1;
	int actualSize = collection.getSize();
	ASSERT_TRUE(actualSize == expectedSize);

}

TEST(RoomCollection, shouldInsertElement) {
	unsigned int size = 10;
	int index = 1;
	RoomCollection collection(size);
	StudyRoom* room = new StudyRoom();
	int expectedLength = 10;
	room->setLength(expectedLength);
	collection.insert(room, index);
	int actualLength = collection[index - 1]->getLength();
	ASSERT_TRUE(actualLength == expectedLength);

}

