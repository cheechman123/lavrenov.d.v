/*
 * testStudyRoom.cpp
 *
 *  Created on: 16 ���. 2018 �.
 *      Author: Daria
 */


#include "gtest/gtest.h"

#include "../src/StudyRoom.h"

TEST (StudyRoom, shouldDefaultInitializeStudyRoom) {

	StudyRoom room;

	char* expectedName = "Empty";
	int expectedSeats = 0;
	roomType expectedType = UNDEFINED;
	char* actualName = room.getName();
	int actualSeats = room.getSeats();
	roomType actualType = room.getType();
	EXPECT_STREQ(expectedName,actualName);
	EXPECT_TRUE(expectedSeats==actualSeats);
	ASSERT_TRUE(expectedType==actualType);

}


TEST (StudyRoom, shouldSetName) {
	StudyRoom room;
	char* expectedName="LAB";
	room.setName(expectedName);
	char* actualName = room.getName();
	ASSERT_STREQ(expectedName, actualName);
}

TEST (StudyRoom, shouldSetSeats) {
	StudyRoom room;
	unsigned int expectedSeats=10;
	room.setSeats(expectedSeats);
	unsigned int actualSeats = room.getSeats();
	ASSERT_TRUE(expectedSeats==actualSeats);
}

TEST (StudyRoom, shouldSetRoomType) {
	StudyRoom room;
	roomType expectedType = LABS;
	room.setType(expectedType);
	roomType actualType = room.getType();
	ASSERT_TRUE(expectedType==actualType);
}

TEST (StudyRoom, shouldAssignUsingOverridedOperator) {
	StudyRoom sr1;
	unsigned int expectedHeight = 10;
		sr1.setHeight(expectedHeight);
		unsigned int expectedLength = 20;
		sr1.setLength(expectedLength);
		unsigned int expectedWidth = 30;
		sr1.setWidth(expectedWidth);
		unsigned int expectedSeats = 15;
		sr1.setSeats(expectedSeats);
		char* expectedName = "Lab";
		sr1.setName(expectedName);
		roomType expectedType=LABS;
		sr1.setType(expectedType);
		StudyRoom sr2;
		sr2 = sr1;
		EXPECT_TRUE(expectedHeight==sr2.getHeight());
		EXPECT_TRUE(expectedLength==sr2.getLength());
		EXPECT_TRUE(expectedWidth==sr2.getWidth());
		ASSERT_STREQ(expectedName,sr2.getName());
		EXPECT_TRUE(expectedSeats==sr2.getSeats());
		EXPECT_TRUE(expectedType==sr2.getType());

}

