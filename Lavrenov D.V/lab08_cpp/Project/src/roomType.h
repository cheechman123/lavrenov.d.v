/*
 * roomType.h
 *
 *  Created on: 19 ����. 2018 �.
 *      Author: Daria
 */

#ifndef SRC_ROOMTYPE_H_
#define SRC_ROOMTYPE_H_

enum roomType {
	PRACTICE, LABS, LECTURES, PROFESSORS, ECONOMIC, UNDEFINED
};

#endif /* SRC_ROOMTYPE_H_ */
