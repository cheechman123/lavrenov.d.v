/*
 * main.cpp
 *
 *  Created on: 14 ����. 2018 �.
 *      Author: Daria
 */
#include "Room.h"

#include "StudyRoom.h"

#include "debug_new.h"
#include "RoomCollection.h"

using namespace std;
/**
 * functional demonastrating
 * @return 0
 */
int main() {
	RoomCollection c(10);
	StudyRoom *r = c[15];
	int indexRight = 6;
	cout << "Try to erase element #" << indexRight << endl;
	c.erase(indexRight);
	cout << "OK" << endl;
	int indexWrong = 15;
	cout << "Try to erase element #" << indexWrong << endl;
	c.erase(indexWrong);
	cout << "OK" << endl;
	return 0;
}

