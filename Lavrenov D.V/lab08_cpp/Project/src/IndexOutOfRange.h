/*
 * IndexOutOfRange.h
 *
 *  Created on: 17 ���. 2018 �.
 *      Author: Daria
 */

#ifndef INDEXOUTOFRANGE_H_
#define INDEXOUTOFRANGE_H_

#include <iostream>

using namespace std;
/**
 * exception out of range
 */
class IndexOutOfRange{

public:
	/**
	 * information about exception
	 */
	void showInfoError(){
		cout << "You try to get element out of collection size!" << endl;
	}
};



#endif /* INDEXOUTOFRANGE_H_ */
