#include <cmath>
#include "iostream"
#include "stdio.h"
#include "string"
using namespace std;

class CWheel{
private:
	int iDiametr;
	int iWidth;
	string sUnit;
public:
	CWheel();
	~CWheel();
	void SetDiam(int aDimetr);
	void SetWidth(int aWidth);
	void SetUnit(string aUnit);
	int GetDiam() const;
	int GetWidth() const;
	string GetUnit() const;
	const int Volume(const CWheel& iWheel) const;
};

class CCar : public CWheel{
private:
	string sRubber;
	string sSize;
public:
	CCar();
	~CCar();
	void SetRubber(string aRubber);
	void SetSize(string aSize);
	string GetRubber() const;
	string GetSize() const;
};

class BaseView{
public:
	virtual void ShowHeader() = 0 ;
	virtual void ShowContent() = 0;
	virtual void ShowFooter() = 0;
	virtual void Display() = 0;
	virtual void PrintData() = 0;
};

class CView1 :public BaseView{
private:
	CCar iCar;
public:
	void SetDataSource(CCar* aCar);
	CView1(CCar acar){
		iCar.SetDiam(acar.GetDiam());
		iCar.SetWidth(acar.GetWidth());
		iCar.SetUnit(acar.GetUnit());
	}
	void PrintData()
	{
	cout<<"������� "<<iCar.GetDiam()<<endl;
	cout<<"������ "<<iCar.GetWidth()<<endl;
	cout<<"������� ��������� "<<iCar.GetUnit()<<endl;
	}
	void ShowHeader(){}
	void ShowContent(){}
	void ShowFooter(){}
	void Display(){}
};
class CView2:public BaseView{
	CCar iCar;
public:
	void SetDataSource(CCar* aCar);
	CView2(CCar acar){
		iCar.SetDiam(acar.GetDiam());
		iCar.SetWidth(acar.GetWidth());
		iCar.SetUnit(acar.GetUnit());
		iCar.SetRubber(acar.GetRubber());
		iCar.SetSize(acar.GetSize());
	}
	void PrintData()
	{
		cout<<"������� "<<iCar.GetDiam()<<endl;
		cout<<"������ "<<iCar.GetWidth()<<endl;
		cout<<"������� ��������� "<<iCar.GetUnit()<<endl;
		cout<<"������ "<<iCar.GetRubber()<<endl;
		cout<<"������ "<<iCar.GetSize()<<endl;
	}
	void ShowHeader(){
		cout<<"\\\\\\ShowHeader///"<<endl;
	}
	void ShowContent(){
		cout<<"\\\\\\ShowContent///"<<endl;
	}
	void ShowFooter(){
		cout<<"\\\\\\ShowFooter///"<<endl;
	}
	void Display(){
		ShowHeader();
		ShowContent();
		ShowFooter();
	}
};
class CView3 :public BaseView{
public:
	CCar iCar;
public:
	CView3(CCar acar){
		iCar.SetDiam(acar.GetDiam());
		iCar.SetWidth(acar.GetWidth());
		iCar.SetUnit(acar.GetUnit());
		iCar.SetRubber(acar.GetRubber());
		iCar.SetSize(acar.GetSize());
	}
	void SetDataSource(CCar* aCar);
	void PrintData();
	void ShowHeader(){}
	void ShowContent(){}
	void ShowFooter(){}
	void Display(){}
};