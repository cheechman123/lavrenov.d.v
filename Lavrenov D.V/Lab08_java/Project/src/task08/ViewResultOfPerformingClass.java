package task08;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


/**
 * ConcreteProduct
 *
 * Contain of Class Converter which
 * performing all acts with Class Number
 * @version 1.0
 * @see View
 */
public class ViewResultOfPerformingClass implements View  {

    /** Define the size of the {@linkplain ViewResultOfPerformingClass#arrayListOfConverters}*/
    protected static final int SIZE_OF_ARRAY_LIST_OF_CONVERTERS = 10;

    /** List of the objects of class {@linkplain Converter}.<br>. */
    protected ArrayList<Converter> arrayListOfConverters = new ArrayList<>();

    /**
     * Constructor of class {@linkplain ViewResultOfPerformingClass}
     * Initializes list {@linkplain ViewResultOfPerformingClass#arrayListOfConverters}
     */
    public ViewResultOfPerformingClass() {
        for(int counterOfCreating = 0;
            counterOfCreating<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfCreating++) {
            arrayListOfConverters.add(counterOfCreating, new Converter());
        }
    }

    /**
     * Returning the collection of objects {@linkplain Converter}
     * @return arrayListOfConverters {@linkplain ArrayList}
     */
    public ArrayList<Converter> getArrayList() {
        return arrayListOfConverters;
    }

    /**
     * Function that generate a random number between 0 and 255
     * @return decimal number
     */
    private int generateDecimalNumberForArrayList() {
        return ((int) (Math.random() * 255));
    }

    /** Realization of method {@linkplain View#initialization()}
     * Setting value of variable of class {@linkplain Converter}
     * of the list {@linkplain ViewResultOfPerformingClass#arrayListOfConverters}
     * {@inheritDoc}
     */
    public void initialization(){
        for(int counterOfInitialization = 0;
            counterOfInitialization<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfInitialization++) {
            arrayListOfConverters.get(counterOfInitialization).init(generateDecimalNumberForArrayList());
        }
    }

    /** Realization of method {@linkplain View#showingOfClass()}
     * Show value of the class {@linkplain Converter} with footer and header
     */
    public void showingOfClass(){
        showHeaderOfClass();
        for(int counterOfShowingOfBody = 0;
            counterOfShowingOfBody<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfShowingOfBody++) {
            arrayListOfConverters.get(counterOfShowingOfBody).showBodyOfNumber();
            showFooterOfClass();
        }
    }

    /** Realization of method {@linkplain View#convertationOfClass()}
     * Doing convertaion class which hidden in {@linkplain Converter}
     */
    public void convertationOfClass(){
        for(int counterOfConvertaion = 0;
            counterOfConvertaion<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfConvertaion++) {
            arrayListOfConverters.get(counterOfConvertaion).convert();
        }
    }

    /** Realization of method {@linkplain View#showHeaderOfClass()}
     * Display header of table
     */
    public void showHeaderOfClass(){
        System.out.println("+------------+-----------+----------+--------+");
        System.out.println("|Decimal     |Binary     |Octal     |Hex     |");
        System.out.println("+------------+-----------+----------+--------+");
    }

    /** Realization of method {@linkplain View#showFooterOfClass()}
     * Display footer of table
     */
    public void showFooterOfClass(){
        System.out.println("+------------+-----------+----------+--------+");
    }

    /** Realization of method {@linkplain View#recordingInFile()}
     * Recording information of variables of class in file
     */
    public void recordingInFile(){
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(Converter.NAMEFILE));
            objectOutputStream.writeObject(arrayListOfConverters);
            objectOutputStream.flush();
            objectOutputStream.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /** Realization of method {@linkplain View#loadingFromFile()}
     * Loading information of variables of class from file
     */
    @SuppressWarnings("unchecked")
    public void loadingFromFile() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(Converter.NAMEFILE));
            try {
                arrayListOfConverters = (ArrayList<Converter>)objectInputStream.readObject();
                objectInputStream.close();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**  Realization of method {@linkplain View#erasingValueOfClass()}
     * Deleting value of variables of class
     */
    public void erasingValueOfClass() {
        for(int counterOfErasingValueOfClass = 0;
            counterOfErasingValueOfClass<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfErasingValueOfClass++) {
            arrayListOfConverters.get(counterOfErasingValueOfClass).erase();
        }
    }

    /** Realization of method {@linkplain View#cleaningOfInstanceOfClass()}
     * Deleting instance of class
     */
    public void cleaningOfInstanceOfClass() {
        for(int counterOfCleaingOfInstanceOfClass = 0;
            counterOfCleaingOfInstanceOfClass<SIZE_OF_ARRAY_LIST_OF_CONVERTERS;
            counterOfCleaingOfInstanceOfClass++) {
            arrayListOfConverters.get(counterOfCleaingOfInstanceOfClass).dispose();
        }
        arrayListOfConverters.clear();
    }
}
