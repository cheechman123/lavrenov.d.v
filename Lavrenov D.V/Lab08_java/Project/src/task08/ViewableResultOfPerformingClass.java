package task08;

/** ConcreteCreator
 *
 * Initializes method that "fabricating" objects
 * @version 1.0
 * @see Viewable
 */
public class ViewableResultOfPerformingClass implements Viewable{

    /** Create object {@linkplain ViewResultOfPerformingClass}*/
    public View getViewOfPerformingClass() {
        return new ViewResultOfPerformingClass();
    }
}
