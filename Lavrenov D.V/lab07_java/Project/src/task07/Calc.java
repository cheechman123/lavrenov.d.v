package task07;


import java.io.Serializable;
import java.util.Formatter;

public class Calc implements Serializable, Basic {

    private String perimeter;
    private String square;
    private String volume;

    @Override
    public String toString() {
        Formatter fmt = new Formatter();
        fmt.format("%-10.9s|%-15.10s|%-15.10s|", perimeter, square, volume);
        return fmt.toString();
    }

    public String Perimeter(int a, int b) {
        perimeter = (2 * (a + b) + "");
        return perimeter;
    }

    public String Volume(int a, int b, int c) {
        volume = (a * b * c + "");
        return volume;
    }

    public String Square(int a, int b) {
        square = (a * b + "");
        return square;
    }

    public String Square(int a) {
        square = (Math.round(Math.PI * Math.pow(a, 2)) + "");
        return square;
    }

    public String getSquare() {
        return square;
    }


}
